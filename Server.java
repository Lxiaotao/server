import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(80);
        while (true) {
            Socket socket = server.accept();
            InputStreamReader r = new InputStreamReader(socket.getInputStream());
            BufferedReader br = new BufferedReader(r);
            String readLine = br.readLine();
            System.out.println(readLine);
            double a ;
            double b ;
            double result = 0;
            while (readLine != null && !readLine.equals("")) {
                if (readLine.contains("GET") && readLine.contains("add")) {
                    Pattern pattern = Pattern.compile("a=\\d+(\\.\\d+)?&?");//匹配小数
                    Matcher matcher = pattern.matcher(readLine);
                    matcher.find();
                    a = Double.parseDouble((matcher.group(0)).replace("a=","").replace("&",""));

                    Pattern pattern1 = Pattern.compile("b=\\d+(\\.\\d+)?");
                    Matcher matcher1 = pattern1.matcher(readLine);
                    matcher1.find();
                    b = Double.parseDouble((matcher1.group(0)).replace("b=",""));
                    System.out.println("a=" + a + "  b=" + b);
                    result=a+b;
                }
                if (readLine.contains("GET") && readLine.contains("mult")) {
                    Pattern pattern = Pattern.compile("a=\\d+(\\.\\d+)?&?");
                    Matcher matcher = pattern.matcher(readLine);
                    matcher.find();
                    a = Double.parseDouble((matcher.group(0)).replace("a=","").replace("&",""));
                    Pattern pattern1 = Pattern.compile("b=\\d+(\\.\\d+)?");
                    Matcher matcher1 = pattern1.matcher(readLine);
                    matcher1.find();
                    b = Double.parseDouble((matcher1.group(0)).replace("b=",""));
                    System.out.println("a=" + a + "  b=" + b);
                    result=a*b;
                }
                readLine = br.readLine();
            }
            String html = "http/1.1 200 ok\n"
                    + "\n"
                    + result;
            PrintWriter pw = new PrintWriter(socket.getOutputStream());
            pw.println(html);
            pw.close();
        }
    }
}